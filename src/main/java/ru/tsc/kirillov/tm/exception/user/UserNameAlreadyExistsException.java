package ru.tsc.kirillov.tm.exception.user;

public final class UserNameAlreadyExistsException extends AbstractUserException {

    public UserNameAlreadyExistsException() {
        super("Ошибка! Пользователь уже существует.");
    }

    public UserNameAlreadyExistsException(final String userName) {
        super("Ошибка! Пользователь с логином `" + userName + "` уже существует.");
    }

}
