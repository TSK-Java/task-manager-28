package ru.tsc.kirillov.tm.exception.field;

import lombok.NoArgsConstructor;
import ru.tsc.kirillov.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractFieldException extends AbstractException {

    public AbstractFieldException(final String message) {
        super(message);
    }

    public AbstractFieldException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(final Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(
            final String message,
            final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
