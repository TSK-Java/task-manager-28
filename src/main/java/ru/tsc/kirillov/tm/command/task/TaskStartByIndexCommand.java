package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Запустить задачу по её индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Запуск задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        getTaskService().changeTaskStatusByIndex(getUserId(), NumberUtil.fixIndex(index), Status.IN_PROGRESS);
    }

}
