package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
