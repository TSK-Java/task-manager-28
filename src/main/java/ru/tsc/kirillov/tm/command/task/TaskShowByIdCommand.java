package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskShowCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить задачу по её ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение задачи по ID]");
        System.out.println("Введите ID задачи:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Task task = getTaskService().findOneById(getUserId(), id);
        showTask(task);
    }

}
