package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.Domain;
import ru.tsc.kirillov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.nio.file.Files;

public final class DataLoadXmlJaxbCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-jaxb";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения из xml файла (JAXB API)";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Загрузка состояния приложения из xml файла (JAXB API)]");
        try (@NotNull InputStream inputStream =
                     Files.newInputStream(getPathFile(FILE_JAXB_XML))) {
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(inputStream);
            setDomain(domain);
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
