package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationAllArgumentCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение списка аргументов.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand cmd: commands) {
            @Nullable final String argument = cmd.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(cmd.getArgument());
        }
    }

}
