package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectShowCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение проекта по ID]");
        System.out.println("Введите ID проекта:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = getProjectService().findOneById(getUserId(), id);
        showProject(project);
    }

}
